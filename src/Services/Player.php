<?php

namespace App\Services;


use App\Entity\Game;
use App\Entity\Question;
use App\Utils\RedisHelper;
use App\Repository\QuestionRepository;
use App\Repository\GameRepository;


class Player
{

	private $redisHelper ;
	private $gameRepository;
	private $questionRepository;


	public function __construct(RedisHelper $redisHelper,  GameRepository $repository, QuestionRepository $questionRepository){

		$this->redisHelper = $redisHelper;
		$this->gameRepository = $repository;
		$this->questionRepository = $questionRepository;


	}

	//evaluate answer and return game with new score
	//$id: gameId
	public function evaluateQuizzResponseFor($id, $content=null){
		if(!$content){
			throw new \Exception("please provide some answer", 1);
		}
		

        $game = $this->gameRepository->findGame($id);
        if ($game->getFinished()){
        	throw new \Exception("this game is finished", 1);
        	
        }

        //fetch number of question created
        $questionCount = $this->questionRepository->getQuestionCount();
      
        if ($questionCount < $content['question']){
        	throw new \Exception("bad question identifier", 1);
      
        }


        $question = $this->questionRepository->findQuestionById($content['question']);

        if(!$question){
        	throw new \Exception("this question id does not exist", 1);
        }

        if($content['response'] === $question->getResponse()){
         
          $game->setScore($game->getScore() + 1);
          $this->redisHelper->saveGame($game);
        }else{
          $game->setFinished(true);
          $this->redisHelper->saveGame($game);
        }
        return $game;
	}
}